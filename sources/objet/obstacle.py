import pygame,sys,os
from pygame.locals import *


class Obstacle(pygame.sprite.Sprite):
    def __init__(self,image,posX,posY):
        pygame.sprite.Sprite.__init__(self) 
        self.image =image
        #self.image.set_colorkey((187, 141, 93))
        self.rect= self.image.get_rect()
        
        self.rect.bottomright = (posX,posY)
        self.mask = pygame.mask.from_surface(self.image)
        
    def update(self,x,y):
        self.rect.move_ip(x, y)
    def draw(self,ecran):
        ecran.blit(self.mask, (0, 0))
        
