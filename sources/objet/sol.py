import pygame,sys,os,math
from pygame.locals import *
#inrtile

class sol(pygame.sprite.Sprite):
    def __init__(self,image,posX,posY,verdure:bool, ecranX,ecranY ,image_herbe):
        self.herbe = pygame.image.load(os.path.join(os.path.dirname(__file__), '../img/Valides',"sol_base_touffes_d'herbes+DESFLEURS+.png")).convert_alpha()
        self.herbe=pygame.transform.scale(self.herbe,(130,99))
        pygame.sprite.Sprite.__init__(self) 
        self.verdure=verdure
        self.image=image
        self.tranformation=False
        #self.image.set_colorkey((187, 141, 93))
        self.rect= self.image.get_rect()
        self.imag_avec_de_l_herb=image_herbe
        self.image_base=image
        self.rect.bottomright = (posX,posY)
        self.mask = pygame.mask.from_surface(self.image)
        self.ecranX= ecranX
        self.ecranY=ecranY 
        self.compter=0
    def update(self,x,y):
        self.rect.move_ip(x, y)
        
        if self.tranformation==True:
            #print("yes")
            if math.sqrt((self.rect.x+50-self.ecranX/2)**2+(self.rect.y-self.ecranY/2)**2)<200 or math.sqrt((self.rect.x-self.ecranX/2)**2+(self.rect.y-self.ecranY/2)**2)<-200 :
                self.image= self.imag_avec_de_l_herb = self.image_base =self.herbe
                #print("wouh hou ")
        self.tranformation=False
        
        if self.compter<= 0:
            posX=self.rect.x
            posY=self.rect.y
            
            self.image=self.image_base
            self.rect= self.image.get_rect()
            self.rect.x =posX
            self.rect.y = posY
        
        if math.sqrt((self.rect.x+50-self.ecranX/2)**2+(self.rect.y-self.ecranY/2)**2)<100 or math.sqrt((self.rect.x-self.ecranX/2)**2+(self.rect.y-self.ecranY/2)**2)<-100 :
            posX=self.rect.x
            posY=self.rect.y
            self.compter=35
            self.image=self.imag_avec_de_l_herb
            self.rect= self.image.get_rect()
            self.rect.x =posX
            self.rect.y = posY
        self.compter-=1
            