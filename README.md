Pour lancer le jeu (Windows et Linux) :
- il faut exécuter le fichier python : principal.py qui se trouve dans le dossier sources
    (par exemple depuis le cmd --> vous pouvez exécuter depuis le fichier sources : py principal.py)

Une fois le jeu lancé, essayez de sauver la ville et faites attention aux bonshommes de pollution. 
Ils sont inoffensifs, mais en groupe, ils peuvent bloquer Eden et vous devrez recommencer (en utilisant le menu pause).

Commandes : Souris (boutons et position), Flèches directonnelles pour bouger et Espace pour utiliser la compétence